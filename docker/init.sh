#!/usr/bin/env bash
cat >/etc/motd <<EOL 
SAMPLE APP FOR ASP.NET CORE 3.1 MEMORY LEAK ON LINUX CONTAINERS
ASP .NETCore Version: `ls -X /usr/share/dotnet/shared/Microsoft.NETCore.App | tail -n 1`
EOL
cat /etc/motd

# from https://github.com/Azure-App-Service/ImageBuilder/blob/master/GenerateDockerFiles/dotnetcore/debian-9/init_container.sh
# Get environment variables to show up in SSH session
eval $(printenv | sed -n "s/^\([^=]\+\)=\(.*\)$/export \1=\2/p" | sed 's/"/\\\"/g' | sed '/=/s//="/' | sed 's/$/"/' >> /etc/profile)

echo "Starting SSH ..."
service ssh start

dotnet /app/aspnetcore3.1memoryleak.dll