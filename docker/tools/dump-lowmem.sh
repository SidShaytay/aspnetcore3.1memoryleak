#!/bin/bash
## Script to facilitate capturing dumps and other trace material
## If invoked without a parameter, will capture data immediately and then
##    stay in a loop to capture again at low memory times
## If invoked with a parameter, will capture by attempting the string param to
##    the filenames and exit immediately

#Minimum available memory limit, MB
THRESHOLD=175

#Check time interval, sec
INTERVAL=15

STORAGEPATH="/storage/"

# param is a friendly name for condition
function getFilename() {
    # `lsb_release` doesn't work in containers
    #local OS=$(lsb_release -d | awk '{printf "%s-%s", $2, $3}')
    local OS=$(uname -r)
    local datetimenow=$(date +%Y-%m-%d_%H-%M-%S)
    local memory=$(free -h | awk '/^Mem:/{print $2}')
    local cpus=$(nproc)

    echo "$STORAGEPATH$datetimenow"-"$OS"-"$cpus"cpus-"$memory"mem-"$1"
}

# param is a friendly name for condition
function captureDumps() {
    # craft fileprefix, 
    filename=$(getFilename "$1")

    # memory dump
    echo "Capturing initial dump"
    echo "Running: dotnet-dump collect -o "$filename".dmp -p "$dotNetPid""
    dotnet-dump collect -o "$filename".dmp -p "$dotNetPid"

    # managed gc dump (disable this for now, it seems to trigger a gen2 GC underneath, ruining the setup ?!?)
    #echo "Capturing initial gcdump"
    #echo "Running: dotnet-gcdump collect -o "$filename".gcdmp -p "$dotNetPid""
    #dotnet-gcdump collect -o "$filename".gcdump -p "$dotNetPid"

    # OS memory situation
    freeOut=$(free -m -w)
    echo "$freeOut" > "$filename"_free.txt

    # dotnet-counters 
    # this will spawn the collection and timeout will kill it in 5 seconds
    # experimentally this works but defi hacky
    echo "Running: timeout --foreground 5 dotnet-counters collect -p "$dotNetPid" --refresh-interval 2 -o "$filename"_dotnet-counters.csv System.Runtime Microsoft.AspNetCore.Hosting"
    timeout --foreground 6 dotnet-counters collect -p "$dotNetPid" -o "$filename"_dotnet-counters.csv System.Runtime Microsoft.AspNetCore.Hosting
}

# Get dotnet processId
dotNetPid=$(dotnet-dump ps| awk '/dotnet / {print $1}')
echo "DotNet PID is" "$dotNetPid"

# Allow for one-shot capture and exit
if [[ "$1" ]]
then
    echo "Capturing traces with tag $1"
    captureDumps $1
    exit 1
fi

# Here means we capture first and then loop ...
captureDumps "freshboot"

while :
do
    # dump to terminal
    freeOut=$(free -m -w)
    echo $(date)
    echo "$freeOut"

    free=$(echo "$freeOut"|awk '/^Mem:/{print $4}')
    buffers=$(echo "$freeOut"|awk '/^Mem:/{print $6}')
    cached=$(echo "$freeOut"|awk '/^Mem:/{print $7}')
    available=$(echo "$freeOut"|awk '/^Mem:/{print $8}')

    if [ $available -lt $THRESHOLD ]
        then
        echo "!! Available memory under" "$THRESHOLD" "MB !! Initiating full memory dump ..."
        captureDumps "lowmem"
        break
    fi

    sleep $INTERVAL

done
