# ASP .NET Core 3.1 Memory Leak

This is a sample repo for Microsoft engineers to investigate a .NET Core 3.1 memory leak. The application the default ASP.NET Core 3.1 template from VS2019 with application insights installed. It's packaged as a linux container.

## Powershell

The build and run scripts are in powershell. To install powershell core for linux, follow https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-7

## Build container

The `Dockerfile` has been tested from the Powershell CLI; untested to build within VS

1. clone this repo by `git clone git@gitlab.com:SidShaytay/aspnetcore3.1memoryleak.git`
2. `sudo pwsh`
3. `cd aspnetcore3.1memoryleak/scripts`
4. `./BuildContainer.ps1`

## Reproduce the issue

It takes between 1 to 4 days to exhaust about 1.5 GB memory. This is seen as high `used` and low `available` memory as see by the `free -w -h` linux tool (which uses the `/proc/meminfo` from container host)

```
Wed Feb 12 15:41:41 UTC 2020
              total        used        free      shared     buffers       cache   available
Mem:           1789        1258          81           5           8         441         378
Swap:             0           0           0
```

### Run the container

While you can the pre-built public container in theory, in practice please clone this repo for the container launch script to setup runtime parameters.

1. clone this repo by `git clone git@gitlab.com:SidShaytay/aspnetcore3.1memoryleak.git`
2. `sudo pwsh`
3. `cd aspnetcore3.1memoryleak/scripts`
4. edit `RunContainer.ps1` and plug in a valid Application Insights by replacing the value of `APPINSIGHTS_INSTRUMENTATIONKEY` around line 31
5. `./RunContainer.ps1` to start the container

### Simulate keep alive

It's observed that the `/` endpoint gets hit about once every 30-45 seconds just at idle (crawlers? load balancer health check?). To simulate this run the `/scripts/ping.alive.sh` script on the HOST which hits that endpoint every second. Unclear if this is an essential step but it's here to perhaps accelerate the leak in ApplicationInsights.

### Capture dumps

1. ssh into container by `ssh root@<DockerHost> -p 22222`. The password is `Docker!`
2. run `/home/dump-lowmem.sh`. This will
    1. capture an initial `dotnet-dump`, kernel memory `free` output, `dotnet-counters` ~~and `dotnet-gcdump`~~ (temporarily disabled since `gcdump` triggers a gen 2 GC run (!?!))
    2. then loop every 30 seconds monitoring for low memory ( under 175MB in script)
    3. when it observes a low memory situation, it'll capture everything in 1. above once again under different timestamped files.
3. dumps saved to `/storage` in container, mapped to the user's home folder in the host (see per `docker run ...` in `RunContainer.ps1`)

### See container app logs

run `sudo docker logs aspnetcore3.1memoryleak:dev` on the host

