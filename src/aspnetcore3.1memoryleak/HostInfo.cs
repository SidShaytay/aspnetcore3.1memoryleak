﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aspnetcore31memoryleak
{
    public class HostInfo
    {
        public static string GetDetails()
        {
            var info = new StringBuilder();
            info.AppendFormat($"Machine name: {Environment.MachineName}" + Environment.NewLine);
            info.AppendFormat($"Memory (working set): {Environment.WorkingSet / 1024 / 1024} MB [IsServerGC:{System.Runtime.GCSettings.IsServerGC} LatencyMode:{System.Runtime.GCSettings.LatencyMode} LOHCMode:{System.Runtime.GCSettings.LargeObjectHeapCompactionMode}]" + Environment.NewLine);
            info.AppendFormat($"CPU Cores: {Environment.ProcessorCount}" + Environment.NewLine);
            info.AppendFormat($"OS Version: {Environment.OSVersion}" + Environment.NewLine);
            return info.ToString();
        }
    }
}
