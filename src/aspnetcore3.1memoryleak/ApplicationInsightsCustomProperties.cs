﻿using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;

namespace aspnetcore31memoryleak
{
    public class ApplicationInsightsCustomProperties : ITelemetryInitializer
    {

        public void Initialize(ITelemetry telemetry)
        {
            var requestTelemetry = telemetry as RequestTelemetry;
            if (requestTelemetry == null)
                return;

            // 400s are NOT service failures; just client originating failures
            var parsed = int.TryParse(requestTelemetry.ResponseCode, out int code);
            if (parsed && code >= 400 && code < 500)
            {
                requestTelemetry.Success = true;
            }
        }
    }
}