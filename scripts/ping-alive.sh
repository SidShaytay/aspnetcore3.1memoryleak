#!/bin/bash

URL="http://localhost:5000/"
INTERVAL=1

i=1
while :
do
    printf "$i | $URL | "
    ((i++))
    curl -sL -w "%{http_code}\\n" "$URL" -o /dev/null
    sleep $INTERVAL
done
