param ([string] $tag = "dev")

Write-Host "For a linux host, do 'sudo pwsh' and then launch this script"

$image = "aspnetcore3.1memoryleak"

Write-Host 'Stopping previous container ...'
docker stop $image-$tag

Write-Host 'Removing previous container ...'
docker rm $image-$tag

Write-Host "Pulling in latest container version from repo ..."
docker pull registry.gitlab.com/sidshaytay/$image`:$tag

Write-Host 'Starting container ...'
docker run -d `
--memory="1.75g" `
--memory-swap="1.75g" `
--cpuset-cpus="0-1" `
-p 5000:5000 `
-p 22222:2222 `
<# container memory dump /storage location maps to ~ on host, adjust as needed #> `
-v $HOME`:/storage `
--name $image-$tag `
-e WEBSITES_ENABLE_APP_SERVICE_STORAGE=true `
-e PORT=5000 `
-e ASPNETCORE_ENVIRONMENT=Development `
-e AllowedHosts='*' `
<# replace with a real application insights instrumentation key #> `
-e APPINSIGHTS_INSTRUMENTATIONKEY=22221111-0000-1111-2222-222233334444 `
--cap-add=SYS_PTRACE ` <# to capturing dumps #> `
registry.gitlab.com/sidshaytay/$image`:$tag

# -e COMPlus_gcServer=0 ` <# 0: workstation, 1: server #>

Write-Host
Write-Host "---------- [ Notes ] ----------"
Write-Host "1. ssh into container by 'ssh root@<DockerHost> -p 22222' , password is 'Docker!' "
Write-Host "2. run '/home/dump-lowmem.sh' within container to capture fresh bootup mem dumps and then periodically"
Write-Host "   monitor low memory (<175MB avail) to auto-trigger low memory situation mem dumps"
Write-Host "3. dumps saved to '/storage' in container, mapped to '~' in host per 'docker run' above"
Write-Host "4. see asp.net core logs from the host by 'docker logs $image`:$tag'"
Write-Host

docker logs $image-$tag