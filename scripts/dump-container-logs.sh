#!/bin/bash

# scripted to strip away ANSI control chars that mess readability in a text editor

sudo docker logs aspnetcore3.1memoryleak-dev | sed 's/\x1b\[[0-9;]*m//g' > container-logs.txt

echo "Container log saved to container-logs.txt in current folder"
