param (
    [string] $tag = "dev",
    [Boolean] $pushImage = $false
)

# Kick off build
$image = "aspnetcore3.1memoryleak"
$dateTag = Get-Date –format 'yyyy-MM-dd_HH-MM-ss'
$solDir = Join-Path $PSScriptRoot ".." -Resolve

# tag all as needed. : needs to be escaped for powershell as `:
& docker build `
    -t $image`:$tag `
    -t $image`:$dateTag `
    -t registry.gitlab.com/sidshaytay/$image`:$tag `
    -t registry.gitlab.com/sidshaytay/$image`:$dateTag `
    -f $solDir/src/aspnetcore3.1memoryleak/Dockerfile `
    $solDir

if (-not $?) {
    throw "Docker build failed with error code $LASTEXITCODE"
}

# Push image to the container registry
if ($pushImage) {
    & docker login registry.gitlab.com

    & docker push registry.gitlab.com/sidshaytay/$image`:$dateTag
    & docker push registry.gitlab.com/sidshaytay/$image`:$tag
}

# Cleanup *system wide* remove all unused containers, images, networks and volumes:
# docker system prune